#!/usr/bin/env node
const path = require('path');

const WebSocket = require('ws');
const http = require('http');
const fs = require('fs');

const sendFile = (res, filePath, type) => {
    const fullFP = `${__dirname}/${filePath}`;

    res.writeHeader(200, { 'Content-Type': type });
    const readStream = fs.createReadStream(fullFP);
    readStream.pipe(res);

}

// HTTP server
const httpServer = http.createServer((req, res) => {
    if ( req.url === '/' ) {
        sendFile(res, 'public/index.html', 'text/html');
        return;
    }
    if ( req.url === '/js/main.js' ) {
        sendFile(res, 'public/js/script.js', 'application/javascript');
        return;
    }

    // req.url === '/' ? sendFile(res, 'public/index.html', 'text/html') : res.end('end');
    // req.url === '/js/main.js' ? sendFile(res, 'public/js/script.js', 'application/javascript') : res.end('end');
    
    res.writeHeader(404);
    res.end();
});

httpServer.listen(8080, function() {
    console.log((new Date()) + ' Server is listening on port 8080 http://localhost:8080');
});

// WS server
const ws = new WebSocket.Server({ server: httpServer });

ws.on('connection', (socket) => {
    console.log((new Date()) + 'ws connection' );
    
    socket.on('message', (data) => {
        console.log(data);
        socket.send(data);
    
    });
});




